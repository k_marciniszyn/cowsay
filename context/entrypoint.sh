#!/bin/bash
if [ $# -eq 0 ]; then
   /usr/games/fortune | /usr/games/cowsay -f vader
else
   /usr/games/cowsay "$@ mouse"
fi
